const fs = require("fs");
const csv = require("csvtojson");
const path = require('path');

const MATCHES_FILE_PATH="../data/matches.csv";
const DELIVERIES_FILE_PATH="../data/deliveries.csv";

const JSON_OUTPUT_DIRECTORY = "../public/output";

const JSON_OUTPUT_FILE_PATH_ARRAY = [
  "teamsWonTheTossAndMatch.json",
  "highestPlayerOfTheMatchAwardsEverySeason.json",
  "strikeRateForEachSeason.json",
  "maximumNoOfDismissed.json",
  "bestEconomyInSuperOver.json"
];

const teamsWonTheTossAndMatch = require("../ipl_module/teamsWonTheTossAndMatch");
const highestPlayerOfTheMatchAwardsEverySeason = require("../ipl_module/highestPlayerOfTheMatchAwardsEverySeason");
const strikeRateForEachSeason = require("../ipl_module/strikeRateForEachSeason");
const maximumNoOfDismissed = require("../ipl_module/maximumNoOfDismissed");
const bestEconomyInSuperOver = require("../ipl_module/bestEconomyInSuperOver");


function main(){
const matches_promise = csv().fromFile(MATCHES_FILE_PATH);
const deliveries_promise = csv().fromFile(DELIVERIES_FILE_PATH);

Promise.all([matches_promise,deliveries_promise]).then(matches_deliveries_array=>{
  const matches = matches_deliveries_array[0];
  const deliveries = matches_deliveries_array[1];

  result_array = [];


  let result_teamsWonTheTossAndMatch = teamsWonTheTossAndMatch(matches);
  result_array.push(result_teamsWonTheTossAndMatch);

  let result_highestPlayerOfTheMatchAwardsEverySeason = highestPlayerOfTheMatchAwardsEverySeason(matches);
  result_array.push(result_highestPlayerOfTheMatchAwardsEverySeason);


  let result_strikeRateForEachSeason = strikeRateForEachSeason(matches, deliveries);
  result_array.push(result_strikeRateForEachSeason);


  let result_maximumNoOfDismissed = maximumNoOfDismissed(deliveries);
  result_array.push(result_maximumNoOfDismissed);

  let result_bestEconomyInSuperOver = bestEconomyInSuperOver(deliveries);
  result_array.push(result_bestEconomyInSuperOver);

  let index = 0;
  result_array.map(result=>{
    let output_path= path.join(JSON_OUTPUT_DIRECTORY , JSON_OUTPUT_FILE_PATH_ARRAY[index]);
    index += 1;
    saveData(result, output_path);
  })

})
.catch(err=>{
  console.error(err)
})

}

function saveData(result, output_path) {

  const jsonData = result;
  const jsonString = JSON.stringify(jsonData);

  fs.writeFile(output_path, jsonString, "utf8", err => {
    if (err) {
      console.error(err);
    }
  });
  
}

main();


