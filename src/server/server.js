const http = require("http");
const url = require("url");
const fs = require("fs");

//  SERVER CODE
const readFilePromise = (filename)=>{
    return new Promise((resolve,reject)=>{
      fs.readFile(filename, 'utf-8', (err, data)=>{
        if(err){
          reject(err);
        }else{
          resolve(data);
        }
      })
    })
  }
  
  
  const errorHandlerCallback = (request, response, err )=>{
    console.error(err);
    response.writeHead(500,{
      'Content-Type' : 'application/json',
    });

    response.write(JSON.stringify({
      message : "Internal server error",
    }))
  response.end();
  }
  
  

  const server= http.createServer((request,response)=>{
  
    switch(request.url){
      case "/":{
        readFilePromise("../public/index.html")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"text/html"})
            response.write(data);
            response.end();
            
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
    case "/style.css":{
      readFilePromise("../public/style.css")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"text/css"})
            response.write(data)
            response.end()
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
    case "/app.js":{
      readFilePromise("../public/app.js")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"text/javascript"})
            response.write(data)
            response.end()
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
    case "/teamsWonTheTossAndMatch.json":{
      readFilePromise("../public/output/teamsWonTheTossAndMatch.json")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"application/json"})
            response.write(data)
            response.end()
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }

    case "/maximumNoOfDismissed.json":{
      readFilePromise("../public/output/maximumNoOfDismissed.json")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"application/json"})
            response.write(data)
            response.end()
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
   

    case "/bestEconomyInSuperOver.json":{
      readFilePromise("../public/output/bestEconomyInSuperOver.json")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"application/json"})
            response.write(data)
            response.end()
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
    

    case "/strikeRateForEachSeason.json":{
      readFilePromise("../public/output/strikeRateForEachSeason.json")
        .then((data)=>{
            response.writeHead(200,{"Content-Type":"application/json"});
            response.write(data);
            response.end();
        })
        .catch((err)=>{
          errorHandlerCallback(request,response,err);
        })
        break;
    }
    

    default:{
        response.writeHead(404,{"Content-Type":"text/html"})
        response.write("<strong>404 Not Found</strong>");
        response.end();
  
    }
    }
  })
  
  server.listen(8080);
  console.log("server is listening on port: 8080......");
  
