function fetchAndVisualizeTeamsWonTheTossAndMatch() {
    fetch("/teamsWonTheTossAndMatch.json")
      .then(response => {
        if(response.ok===true){
          return response.json();
        }else{
          throw new Error("IT IS NOT OK");
        }
      })
      .then(visualizeTeamsWonTheTossAndMatch)
      .catch(err=>{
        console.error(err);
      });
  }


  fetchAndVisualizeTeamsWonTheTossAndMatch();


function visualizeTeamsWonTheTossAndMatch(data) {
  const seriesData=[];
  for(let team in data){
    seriesData.push([team, data[team]]);
  }

  Highcharts.chart("teams-Won-The-Toss-And-Match", {
    chart: {
      type: "column"
    },
    title: {
      text: "teams-Won-The-Toss-And-Match"
    },
    subtitle: {
      text:
        'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
    },
    xAxis: {
      type: "category"
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches"
      }
    },
    series: [
      {
        name: "Team",
        data: seriesData
      }
    ]
  });

}



//SECOND PROBLEM
function fetchAndVisualizeMaximumNoOfDismissed() {
  fetch("/maximumNoOfDismissed.json")
    .then(response => {
      if(response.ok===true){
        return response.json();
      }else{
        throw new Error("IT IS NOT OK");
      }
    })
    .then(visualizeMaximumNoOfDismissed)
    .catch(err=>{
      console.error(err);
    });
}

fetchAndVisualizeMaximumNoOfDismissed();


function visualizeMaximumNoOfDismissed(data) {
  const seriesData=[];
  for(let number in data){
    seriesData.push([`${data[number].batsman} dismissed-by ${data[number].dismissed_by}`, data[number].no_of_times]);
  }

  Highcharts.chart("maximum-No-Of-Dismissed", {
    chart: {
      type: "column"
    },
    title: {
      text: "maximum-No-Of-Dismissed"
    },
    subtitle: {
      text:
        'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
    },
    xAxis: {
      type: "Player Dismissed By"
    },
    yAxis: {
      min: 0,
      title: {
        text: "Number Of Times"
      }
    },
    series: [
      {
        name: "Player Dismissed By",
        data: seriesData
      }
    ]
  });

}


//THIRD PROBLEM
function fetchAndVisualizeBestEconomyInSuperOver() {
  fetch("/bestEconomyInSuperOver.json")
    .then(response => {
      if(response.ok===true){
        return response.json();
      }else{
        throw new Error("IT IS NOT OK");
      }
    })
    .then(visualizeBestEconomyInSuperOver)
    .catch(err=>{
      console.error(err);
    });
}

fetchAndVisualizeBestEconomyInSuperOver();


function visualizeBestEconomyInSuperOver(data) {
  const seriesData=[];
  for(let number in data){
    seriesData.push([data[number].Bowler, Number(data[number].Economy)]);
  }

  Highcharts.chart("best-Economy-In-Super-Over", {
    chart: {
      type: "column"
    },
    title: {
      text: "best-Economy-In-Super-Over"
    },
    subtitle: {
      text:
        'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
    },
    xAxis: {
      type: "Bowler"
    },
    yAxis: {
      min: 0,
      title: {
        text: "Economy"
      }
    },
    series: [
      {
        name: "Bowler's Economy",
        data: seriesData
      }
    ]
  });

}



//FOURTH PROBLEM
function fetchAndVisualizeStrikeRateForEachSeason() {
  fetch("/strikeRateForEachSeason.json")
    .then(response => {
      if(response.ok===true){
        return response.json();
      }else{
        throw new Error("IT IS NOT OK");
      }
    })
    .then(visualizeStrikeRateForEachSeason)
    .catch(err=>{
      console.error(err);
    });
}

fetchAndVisualizeStrikeRateForEachSeason();


function visualizeStrikeRateForEachSeason(data) {
  const seriesData=[];
  const all_batsman = [];
  const season = Object.keys(data).map((season) => season);
  for (let index = 0; index < season.length; index++) {
    all_batsman.push(Object.keys(data[season[index]]));
  }
  const unique_batsman = [...new Set([].concat.apply([], all_batsman))];
  for (let i in unique_batsman) {
    let Array = [];
    for (let j in season) {
      if (data[season[j]].hasOwnProperty(unique_batsman[i])) {
        Array.push(Number(data[season[j]][unique_batsman[i]]));
      }else {
        Array.push(0);
      }
    }
    seriesData.push({ name: unique_batsman[i], data: Array });
  }
  console.log(seriesData);

  Highcharts.chart('strike-Rate-For-Each-Season', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'strike-Rate-For-Each-Season'
    },
    subtitle: {
      text: 'Source: ipl.com'
    },
    xAxis: {
      categories: [...season],
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Strike Rate :'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: seriesData
  });

}