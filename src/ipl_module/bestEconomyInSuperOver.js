function bestEconomyInSuperOver(deliveries){

    let bowlers_with_runs_and_balls = {};
        deliveries.map(delivery => {

        if(delivery.is_super_over == '1'){
            if(bowlers_with_runs_and_balls[delivery.bowler] !== undefined){

                let run = (parseInt(delivery.total_runs)-parseInt(delivery.bye_runs)-
                    parseInt(delivery.legbye_runs));

                bowlers_with_runs_and_balls[delivery.bowler].runs += run;

                if(delivery.wide_runs == '0' && delivery.noball_runs == '0'){

                    bowlers_with_runs_and_balls[delivery.bowler].balls += 1;

                } 
            }else{
                let ball = 0;
                let run = (parseInt(delivery.total_runs)-parseInt(delivery.bye_runs)-
                    parseInt(delivery.legbye_runs));

                if(delivery.wide_runs == '0' && delivery.noball_runs == '0'){
                    ball=1;
                }
                bowlers_with_runs_and_balls[delivery.bowler] = {
                    runs : run,
                    balls : ball
                }
            }
        }
    });
    let array = [];
    for(let Bowler in bowlers_with_runs_and_balls){
        
        let economy = ((bowlers_with_runs_and_balls[Bowler].runs / 
                bowlers_with_runs_and_balls[Bowler].balls)*6).toFixed(2);

        array.push([Bowler, economy]);

    }
    //only 1 index containing the economy
    array.sort((oneBowler,nextBowler) => oneBowler[1]-nextBowler[1]);

    let bowler_with_economy = {};

    let index =1;
    array.slice(0,10).map(bowler_array=>{
        bowler_with_economy[index]={

            //0 index contains bowler name and 1 index contains economy
            Bowler : bowler_array[0],
            Economy : bowler_array[1]

        }
        index += 1;
    })


    return bowler_with_economy;
}

module.exports = bestEconomyInSuperOver;