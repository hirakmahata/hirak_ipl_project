
function playersOfTheMatches(year, matches){
    let players_of_the_match =  matches.reduce((accumulator_result,match) => {
        if(match.season == year){
            if(accumulator_result[match.player_of_match] !== undefined){
                accumulator_result[match.player_of_match] += 1;
            }else{
                accumulator_result[match.player_of_match] = 1;
            }
        }
        return accumulator_result;
    },{});

    let temp_array = [];
    for(let player in players_of_the_match){

        temp_array.push([player, players_of_the_match[player]]);

    }
    //only 1 index is containing the  number of awards
    temp_array.sort((onePlayer,nextPlayer) => nextPlayer[1] - onePlayer[1]); 

    //taking highest award winner after sorting
    let player = temp_array[0][0];

    return player;
}


function highestPlayerOfTheMatchAwardsEverySeason(matches){

    let result = {};
    for(let match of matches){
        if(result[match.season] == undefined){

            let year = match.season;
            result[match.season] = playersOfTheMatches(year,matches);

        }
    }

    return result;
}



module.exports = highestPlayerOfTheMatchAwardsEverySeason;