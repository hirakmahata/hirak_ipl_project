function maximumNoOfDismissed(deliveries){
    let dismissed_players = deliveries.reduce((accumulatorPlayers,delivery) => {

        if(delivery.player_dismissed != ''){

           if(accumulatorPlayers[delivery.player_dismissed] !== undefined){

               if(accumulatorPlayers[delivery.player_dismissed][delivery.bowler] !== undefined){

                accumulatorPlayers[delivery.player_dismissed][delivery.bowler] += 1;

               }else{

                accumulatorPlayers[delivery.player_dismissed][delivery.bowler] = 1;

               }

           }else{

            accumulatorPlayers[delivery.player_dismissed] = {};
            accumulatorPlayers[delivery.player_dismissed][delivery.bowler] = 1;

           }

        }
        return accumulatorPlayers;

    },{});

    let temp_array = [];
    for(let Batsman in dismissed_players){

        for(let Bowler in dismissed_players[Batsman]){

            temp_array.push([Batsman, Bowler, dismissed_players[Batsman][Bowler]]);

        }

    }
    //only index 2 contains dismissal number
    temp_array.sort((oneDismissalNumber,nextDismissalNumber) => 
        nextDismissalNumber[2]-oneDismissalNumber[2]);  

    let topTenDismissal = {};
    let index=1;
    temp_array.slice(0,10).map(batsmanAndBowlerArray=>{
        topTenDismissal[index] = {
     // 0 index contains batsman name, 1 contains bowler name and 2 contains number of times
            batsman : batsmanAndBowlerArray[0],
            dismissed_by : batsmanAndBowlerArray[1],
            no_of_times : batsmanAndBowlerArray[2]

        }
        index += 1;
    })

    
    return topTenDismissal;
    
}

module.exports = maximumNoOfDismissed;