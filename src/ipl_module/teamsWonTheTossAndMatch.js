
function teamsWonTheTossAndMatch(matches){

    let results_of_teams = matches.reduce((accumulator_result,match)=>{
         
        if(match.toss_winner == match.winner){

            if(accumulator_result[match.winner] != undefined){
                accumulator_result[match.winner] += 1;
            }else{
                accumulator_result[match.winner] = 1;
            }
        }
        return accumulator_result;
    },{})
    return results_of_teams;
}

module.exports = teamsWonTheTossAndMatch;