
function batsmanWithRunsAndBalls(year,matches,deliveries){
    let batsman_details = {};

        matches.map(match => {
        if(match.season == year){
                deliveries.map(delivery => {
                if(match.id == delivery.match_id){
                    if(batsman_details[delivery.batsman] !== undefined){

                        batsman_details[delivery.batsman].runs += parseInt(delivery.batsman_runs);
                        batsman_details[delivery.batsman].balls += 1;

                    }else{

                        batsman_details[delivery.batsman] = {

                            runs : parseInt(delivery.batsman_runs),
                            balls : 1

                        };

                    }
                }
            })
        }
    });

    let output = {};
    let allBatsmanArray = [];
    for(let batsman in batsman_details){

        let run = batsman_details[batsman].runs;
        let ball = batsman_details[batsman].balls;
        let strike_rate = ((run/ball)*100).toFixed(2);
        allBatsmanArray.push([batsman,strike_rate]);

    }
    //1 index contains strike rate
    allBatsmanArray.sort((oneBatsman,nextBatsman) => 
        nextBatsman[1]-oneBatsman[1]);

        allBatsmanArray.map(batsmanArray=>{
            output[batsmanArray[0]] = batsmanArray[1];
        })

    return output;
}


function strikeRateForEachSeason(matches, deliveries){
    let result = {};

        matches.map(match => {

        if(result[match.season] == undefined){

            let year = match.season;
            result[match.season] = batsmanWithRunsAndBalls(year,matches,deliveries);

        }

    })

    return result;
}

module.exports = strikeRateForEachSeason;